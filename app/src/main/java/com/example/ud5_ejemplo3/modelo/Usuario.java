package com.example.ud5_ejemplo3.modelo;

import com.google.gson.annotations.SerializedName;

public class Usuario {
    @SerializedName("id")
    private String id;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("imagen")
    private String imagen;

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getImagen() {
        return imagen;
    }
}
