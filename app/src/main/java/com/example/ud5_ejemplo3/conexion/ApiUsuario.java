package com.example.ud5_ejemplo3.conexion;

import com.example.ud5_ejemplo3.modelo.Usuario;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiUsuario {
    @GET("usuario")
    Call<Usuario> obtenerUsuario();
}
