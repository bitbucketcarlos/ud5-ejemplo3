package com.example.ud5_ejemplo3;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ud5_ejemplo3.conexion.ApiUsuario;
import com.example.ud5_ejemplo3.conexion.Cliente;
import com.example.ud5_ejemplo3.modelo.Usuario;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofit = Cliente.obtenerCliente();

        obtenerDatos();
    }

    private void obtenerDatos(){

        ApiUsuario api = retrofit.create(ApiUsuario.class);

        // Obtenemos la respuesta.
        Call<Usuario> respuesta = api.obtenerUsuario();

        // Realizamos una petición asíncrona y debemos implementar un Callback con dos métodos:
        // onResponse y onFailure.
        respuesta.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                // Si la respuesta es correcta accedemos al cuerpo del mensaje recibido,
                // extraemos la información del usuario.
                if(response.isSuccessful()){
                    Usuario respuesta = response.body();

                    TextView nombre = findViewById(R.id.nombre);
                    ImageView imagen = findViewById(R.id.imagen);

                    nombre.setText(respuesta.getNombre());

                    // Obtenemos la imagen usando la ruta de la respuesta y la insertamos en el ImageView del layout.
                    Glide.with(MainActivity.this).load(respuesta.getImagen()).into(imagen);

                }
                else{
                    Toast.makeText(getApplicationContext(), "Fallo en la respuesta", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}