# Ud5_Ejemplo3
_Ejemplo 3 de la Unidad 5._

Vamos a implementar una aplicaci�n que se conecte a nuestro servidor y permita obtener la imagen de perfil del usuario. 

Para ello deberemos crear una carpeta llamada _assets_ dentro de la carpeta _public_ del servidor y dentro de �sta otra llamada _images_. 
La ruta para la imagen ser�a algo como _..\json-server-master\public\assets\images\perfil.jpg_

Y el fichero _db.json_ deber� contener un objeto con la siguiente estructura:

```json
...
 "usuario":
    {
      "id": 1,
      "nombre": "Pepe",
      "imagen": "http://10.0.2.2:3000/assets/images/perfil.jpg"
    },
...
```

Los pasos ser�n los siguientes:

## Paso 1: Modificar el fichero _build.gradle(:app)_
A�adiremos las siguientes dependencias para poder hacer uso de la librer�a _Glide_ para obetner las im�genes, adem�s de la de _Retrofit_, del convertidor de _JSON_ _GSON_ y del _RecyclerView_
(Podemos ver la versi�n actual en _https://github.com/square/retrofit_):
```html
    implementation 'androidx.recyclerview:recyclerview:1.2.1'
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    implementation 'com.github.bumptech.glide:glide:4.12.0'
```

Adem�s, para su correcto funcionamiento, deberemos comprobar que las siguientes l�neas estas en el bloque _android_:
```html
android {
    ...
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    ...
}
```

## Paso 2: Dar permisos a la aplicaci�n
Para poder realizar conexiones con servidores deberemos darle permisos a la aplicaci�n. Para ello se debe a�adir la siguiente l�nea en el fichero _AndrodManifest.xml_:
```html
<uses-permission android:name="android.permission.INTERNET"/>
```

## Paso 3: Creaci�n de la interfaz _ApiUsuario_
Creamos una interfaz con los m�todos HTTP a utilizar en nuestra aplicaci�n. 
```java
public interface ApiUsuario {
    @GET("usuario")
    Call<Usuario> obtenerUsuario();
}

```

## Paso 4: Creaci�n de la clase _Cliente_
Creamos una clase en la que construiremos el objeto _Retrofit_. En la constante _URL_ deberemos asignar la direcci�n _IP_ de 
nuestro servidor.
```java
public class Cliente {
    // Direcci�n URL del servidor json server.
    private static final String URL = "http://10.0.2.2:3000";
    private static Retrofit retrofit = null;

    public static Retrofit obtenerCliente(){
        if(retrofit == null){
            // Construimos el objeto Retrofit asociando la URL del servidor y el convertidor Gson
            // para formatear la respuesta JSON.
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}
```

## Paso 5: Creaci�n de la clase _Usuario_
Creamos un _POJO_ con los datos que queremos obtener del usuario.
```java
public class Usuario {
    @SerializedName("id")
    private String id;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("imagen")
    private String imagen;

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getImagen() {
        return imagen;
    }
}
```

## Paso 6: Creaci�n del _layout_ _activity_main.xml_
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/nombre"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintHorizontal_bias="0.498"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.258"
        tools:text="Usuario" />

    <ImageView
        android:id="@+id/imagen"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/nombre" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

## Paso 7: Creaci�n de la clase _MainActivity_
Por �ltimo, en la clase _MainActivity_ haremos uso de la librer�a Glide para obtener la imagen del servidor.
```java
public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofit = Cliente.obtenerCliente();

        obtenerDatos();
    }

    private void obtenerDatos(){

        ApiUsuario api = retrofit.create(ApiUsuario.class);

        // Obtenemos la respuesta.
        Call<Usuario> respuesta = api.obtenerUsuario();

        // Realizamos una petici�n as�ncrona y debemos implementar un Callback con dos m�todos:
        // onResponse y onFailure.
        respuesta.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                // Si la respuesta es correcta accedemos al cuerpo del mensaje recibido,
                // extraemos la informaci�n del usuario.
                if(response.isSuccessful()){
                    Usuario respuesta = response.body();

                    TextView nombre = findViewById(R.id.nombre);
                    ImageView imagen = findViewById(R.id.imagen);

                    nombre.setText(respuesta.getNombre());

                    // Obtenemos la imagen usando la ruta de la respuesta y la insertamos en el ImageView del layout.
                    Glide.with(MainActivity.this).load(respuesta.getImagen()).into(imagen);

                }
                else{
                    Toast.makeText(getApplicationContext(), "Fallo en la respuesta", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
```